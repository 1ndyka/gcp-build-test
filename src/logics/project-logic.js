import * as projectRepository from '../repositories/project-repository';

const getProjects = async () => {
	const projects = await projectRepository.getProjects();
	return projects;
};

export {
	getProjects,
};
