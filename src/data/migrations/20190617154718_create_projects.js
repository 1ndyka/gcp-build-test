exports.up = knex => knex.schema.createTable('projects', (t) => {
	t.increments('id').primary().unsigned();
	t.string('name');
	t.text('description');
	t.integer('user_id').unsigned()
		.index()
		.references('id')
		.inTable('users');
	t.timestamp('completed_at');
	t.timestamp('created_at').defaultTo(knex.fn.now());
	t.timestamp('updated_at').defaultTo(knex.fn.now());
});

exports.down = knex => knex.schema.dropTable('projects');
