/**
 * Created by WebStorm.
 * User: darmawanefendi
 * Date: 2019-06-17
 * Time: 15:23
 */

import knex from 'knex';
import knexConfig from '../../knexfile';
import { variable } from '../helpers';

export default knex(knexConfig[variable.current_env]);
