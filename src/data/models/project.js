import { model } from '../../helpers';

const name = 'User';
const tableName = 'users';

const selectableProps = [
	'id',
	'name',
	'created_at',
];

module.exports = (knex) => {
	const projectModel = model({
		knex,
		name,
		tableName,
		selectableProps,
	});

	return {
		...projectModel,
	};
};
