export const errorResponse = errors => ({
	errors,
});

export const successResponse = (code, status, msg, data) => ({
	meta: {
		code,
		status,
		message: msg,
	},
	data,
});
