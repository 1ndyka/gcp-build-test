/**
 * Created by WebStorm.
 * User: darmawanefendi
 * Date: 2019-06-17
 * Time: 13:40
 */
module.exports = {
	current_env: process.env.NODE_ENV,
	env: {
		local: 'localhost',
		development: 'development',
		staging: 'staging',
		production: 'production',
		test: 'test',
	},
	development: {
		xApiKey: 'secret-xApiKey-for-developer',
		xDeviceNotificationCode: 'secret-xDeviceNotificationCode-for-developer',
	},
};
