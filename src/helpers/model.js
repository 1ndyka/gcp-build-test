// The guts of a model that uses Knexjs to store and retrieve data from a
// database using the provided `knex` instance. Custom functionality can be
// composed on top of this set of common guts.
//
// The idea is that these are the most-used types of functions that most/all
// "models" will want to have. They can be overriden/modified/extended if
// needed by composing a new object out of the one returned by this function ;)

module.exports = ({
	knex = {},
	name = 'name',
	tableName = 'tablename',
	selectableProps = [],
	timeout = 1000,
}) => {
	const create = (props) => {
		// eslint-disable-next-line no-param-reassign
		delete props.id; // not allowed to set `id`

		return knex.insert(props)
			.into(tableName)
			.timeout(timeout);
	};

	const findAll = (props = null) => knex.select(props || selectableProps)
		.from(tableName)
		.timeout(timeout);

	const find = (filters = {}, props = null) => knex.select(props || selectableProps)
		.from(tableName)
		.where(filters)
		.timeout(timeout);

	// Same as `find` but only returns the first match if >1 are found.
	const findOne = (filters, props = null) => find(filters, props)
		.then((results) => {
			if (!Array.isArray(results)) return results;
			return results[0];
		});

	const findById = id => knex.select(selectableProps)
		.from(tableName)
		.where({ id })
		.timeout(timeout)
		.then((results) => {
			if (!Array.isArray(results)) return results;
			return results[0];
		});

	const update = (id, props) => {
		// eslint-disable-next-line no-param-reassign
		delete props.id; // not allowed to set `id`

		return knex.update(props)
			.from(tableName)
			.where({ id })
			.returning(selectableProps)
			.timeout(timeout);
	};

	const destroy = id => knex.del()
		.from(tableName)
		.where({ id })
		.timeout(timeout);

	const query = () => knex.from(tableName)
		.timeout(timeout);

	return {
		name,
		tableName,
		selectableProps,
		timeout,
		create,
		findAll,
		find,
		findOne,
		findById,
		update,
		destroy,
		query,
	};
};
