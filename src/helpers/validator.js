import _ from 'lodash';
import * as errors from './errors';
import { utilities } from '.';

const validatorWrapper = fn => (body, key, value) => {
	try {
		fn(body, key, value);
		return { data: true, error: null };
	} catch (e) {
		return { data: null, error: e };
	}
};

export const required = validatorWrapper((body, key) => {
	if (!_.has(body, key)) throw new errors.internalError.InCompleteKeyError(key, null);
	if (body[key] === '' || body[key] === null) throw new errors.internalError.InCompleteKeyError(key, null);
});


export const number = validatorWrapper((body, key) => {
	if (!utilities.isNumber(Number(body[key]))) {
		throw new errors.internalError.InvalidTypeError(key, null);
	}
});
//
// export const integer = (res, body, key) => {
// 	if (!utilities.isInteger(Number(body[key]))) {
// 		return response.badRequestType(res, messages.invalidType(key), {});
// 	}
// 	return true;
// };
//
// export const phone = (res, body, key) => {
// 	if (!utilities.validatePhoneNumber(body[key])) {
// 		return response.badRequestFormat(res, messages.invalidFormat(key), {});
// 	}
// 	return true;
// };
//
// export const email = (res, body, key) => {
// 	if (!utilities.validateEmail(body[key])) {
// 		return response.badRequestFormat(res, messages.invalidFormat(key), {});
// 	}
// 	return true;
// };
//
// export const max = (res, body, key, value) => {
// 	if (!Number.isNaN(Number(body[key]))) {
// 		if (Number(body[key]) > value) {
// 			return response.badRequestValue(res, messages.invalidMaxValue(key, value), {});
// 		}
// 	} else if (body[key].length > value) {
// 		return response.badRequestValue(res, messages.invalidMaxLength(key, value), {});
// 	}
// 	return true;
// };
//
// export const min = (res, body, key, value) => {
// 	if (!Number.isNaN(Number(body[key]))) {
// 		if (Number(body[key]) < value) {
// 			return response.badRequestValue(res, messages.invalidMinValue(key, value), {});
// 		}
// 	} else if (body[key].length < value) {
// 		return response.badRequestValue(res, messages.invalidMinLength(key, value), {});
// 	}
// 	return true;
// };
