/**
 * Created by WebStorm.
 * User: darmawanefendi
 * Date: 2019-06-17
 * Time: 13:40
 */

import flatted from 'flatted';
import { logger } from '../modules';

const getDataContext = res => ({
	requestId: res.locals.requestId,
	requestDate: res.locals.requestDate,
	// user: flatted.stringify(res.locals.user),
});

export const info = (message, data) => {
	logger.info(message, { dataContext: flatted.stringify(data) });
};

export const warn = (message, data) => {
	logger.warn(message, { dataContext: flatted.stringify(data) });
};

export const error = (message, data) => {
	logger.error(message, { dataContext: flatted.stringify(data) });
};

export const verbose = (message, data) => {
	logger.verbose(message, { dataContext: flatted.stringify(data) });
};

export const debug = (message, data) => {
	logger.debug(message, { dataContext: flatted.stringify(data) });
};

export const silly = (message, data) => {
	logger.silly(message, { dataContext: flatted.stringify(data) });
};

export const infoWithContext = (res, message, data) => {
	const dataContext = getDataContext(res);
	const newDataContext = Object.assign(dataContext, data);
	logger.info(message, { dataContext: flatted.stringify(newDataContext) });
};

export const errorWithContext = (res, message, data) => {
	const dataContext = getDataContext(res);
	const newDataContext = Object.assign(dataContext, data);
	logger.error(message, { dataContext: flatted.stringify(newDataContext) });
};

export const warnWithContext = (res, message, data) => {
	const dataContext = getDataContext(res);
	const newDataContext = Object.assign(dataContext, data);
	logger.warn(message, { dataContext: flatted.stringify(newDataContext) });
};

export const verboseWithContext = (res, message, data) => {
	const dataContext = getDataContext(res);
	const newDataContext = Object.assign(dataContext, data);
	logger.verbose(message, { dataContext: flatted.stringify(newDataContext) });
};

export const debugWithContext = (res, message, data) => {
	const dataContext = getDataContext(res);
	const newDataContext = Object.assign(dataContext, data);
	logger.debug(message, { dataContext: flatted.stringify(newDataContext) });
};

export const sillyWithContext = (res, message, data) => {
	const dataContext = getDataContext(res);
	const newDataContext = Object.assign(dataContext, data);
	logger.silly(message, { dataContext: flatted.stringify(newDataContext) });
};

export const startProfile = (res, name, data) => {
	const dataContext = getDataContext(res);
	const newDataContext = Object.assign(dataContext, data);
	logger.profile(name, { dataContext: flatted.stringify(newDataContext) });
};

export const endProfile = (res, name, data) => {
	const dataContext = getDataContext(res);
	const newDataContext = Object.assign(dataContext, data);
	logger.profile(name, { dataContext: flatted.stringify(newDataContext) });
};
