/**
 * Created by WebStorm.
 * User: darmawanefendi
 * Date: 2019-06-18
 * Time: 15:33
 */

import * as httpError from './httpError';
import * as internalError from './internalError';


export {
	httpError,
	internalError,
};
