/**
 * Created by WebStorm.
 * User: darmawanefendi
 * Date: 2019-06-17
 * Time: 13:56
 */
import uuid from 'uuid';
import _ from 'lodash';
import { logger, utilities } from '../helpers';

const initiate = (req, res, next) => {
	// generate request id
	const requestHeaders = utilities.lowerCaseKeyObject(req.headers);
	if (_.has(requestHeaders, 'x-request-id')) {
		if (requestHeaders['x-request-id'] === '' || requestHeaders['x-request-id'] === null) res.locals.requestId = uuid('v4');
		else {
			res.locals.requestId = requestHeaders['x-request-id'];
		}
	} else {
		res.locals.requestId = uuid('v4');
	}
	res.locals.requestDate = new Date();

	logger.infoWithContext(res, '=== Request Started ===', {
		headers: JSON.stringify(req.headers),
		method: req.method,
		originalUrl: req.originalUrl,
		query: JSON.stringify(req.query),
		body: JSON.stringify(req.body),
	});

	logger.startProfile(res, 'MAIN_REQUEST_PROFILING');

	res.on('finish', () => {
		logger.infoWithContext(res, '=== Request Ended ===');
		logger.endProfile(res, 'MAIN_REQUEST_PROFILING');
	});
	next();
};

export {
	initiate,
};
