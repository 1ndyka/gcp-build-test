import express from 'express';
import helmet from 'helmet';
import compression from 'compression';
import cors from 'cors';
import v1Routes from './routes';
import {
	parsers, responses, errors, logger,
} from './helpers';
import { requestMiddleware } from './middlewares';

const app = express();

app.use(cors());
app.use(helmet());
app.use(compression());

app.enable('trust proxy');

app.use(parsers.jsonParser);
app.use(parsers.urlencodedExtendedParser);

app.use(requestMiddleware.initiate);
// app.use(headerMiddleware.requiredKeys);

app.use('/api/v1/', v1Routes);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
	next(errors.httpError.notFound(new errors.internalError.ResourceNotFoundError('URL', null)));
});

// All errors must into this handler
// This must use eslint-disable-line because without next params, this method will be no executed
app.use((err, req, res, next) => { //eslint-disable-line
	// restructure error messages
	const appErrors = [];
	const data = err.errors;
	if (Array.isArray(data)) {
		for (let i = 0; i < data.length; i += 1) {
			appErrors.push(data[i].data);
		}
	} else {
		appErrors.push(data.data);
	}
	logger.errorWithContext(res, '=== Caught error exception ===', { status: err.status || 500, errors: appErrors });
	responses.httpResponse.errorHandler(res, err.status || 500, appErrors);
});

export default app;
