// eslint-disable-next-line import/named
import { Project } from '../models';

const getProjects = async () => {
	const projects = await Project.findAll();
	return projects;
};

export {
	getProjects,
};
