import { Router } from 'express';
import global from './global';

const routes = Router();

global(routes);

export default routes;
