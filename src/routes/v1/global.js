import { v1Controller } from '../../controllers';

export default (routes) => {
	routes.get('/hello', v1Controller.global.helloWorld);

	// get to db examples
	routes.get('/project', v1Controller.global.project.index);
};
