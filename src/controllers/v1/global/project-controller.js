import {
	responses,
} from '../../../helpers';
import * as projectLogic from '../../../logics/project-logic';

// NOTE: Because we support multiple error messages
// we have 2 error types: 1. Internal error (with custom code) and HttpError (with http code)
// so on level business logic (logic) we must throw internal error type like func validators.required and validators.number
// and we wrap that error with HttpError on controller (on line 17)

const index = async (req, res) => {
	// ======================= Comment this line if you want to success response =====================
	// const errs = [];
	// const val1 = validators.required(req.body, 'key1', null); // <-- ex func return internal error
	// if (val1.error) errs.push(val1.error);
	// const val2 = validators.number(req.body, 'key2', null); // <- ex func return internal error
	// if (val2.error) errs.push(val2.error);
	// if (errs.length > 0) throw errors.httpError.badRequest(errs); // <- ex func return httpError
	// ======================= Comment this line if you want to success response =====================

	const projects = await projectLogic.getProjects();

	return responses.httpResponse.ok(res, 'OK', {
		data: projects,
	});
};

export {
	index,
};
