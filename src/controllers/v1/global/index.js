import helloWorldController from './hello-world-controller';
import * as projectController from './project-controller';

module.exports = {
	helloWorld: helloWorldController,
	project: projectController,
};
