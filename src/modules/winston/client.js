/**
 * Created by WebStorm.
 * User: darmawanefendi
 * Date: 2019-06-17
 * Time: 13:39
 */

import winston from 'winston';
import config from 'config';
import fluentLogger from 'fluent-logger';
import variable from '../../helpers/variable';

const transports = [];

if (variable.current_env === variable.env.local || variable.current_env === variable.env.test) {
	transports.push(new winston.transports.Console({
		level: 'debug',
		handleExceptions: true,
		json: false,
		silent: variable.current_env === variable.env.test,
		colorize: true,
	}));
}

if (config.fluentd.is_enabled) {
	const hostConfig = {
		host: config.fluentd.host,
		port: config.fluentd.port,
		timeout: 3.0,
	};

	const FluentTransport = fluentLogger.support.winstonTransport();
	transports.push(new FluentTransport(config.fluentd.tag, hostConfig), new (winston.transports.Console)());
}


const logger = winston.createLogger({
	transports,
	exitOnError: false,
});

export default logger;
