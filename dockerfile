FROM node:alpine

WORKDIR /app
COPY package*.json /app/
RUN apk add --no-cache make gcc g++ python && \
    npm install --production && \
    apk del make gcc g++ python
COPY . .
CMD ["node", "bin/www"]

EXPOSE 7003